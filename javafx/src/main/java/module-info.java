module snippets.jogl.javafx
{
	exports snippets.jogl.javafx;

	requires javafx.base;
	requires javafx.controls;
	requires transitive javafx.graphics;

	requires transitive jogamp.fat;
}