package snippets.jogl.javafx;

import com.jogamp.newt.NewtFactory;
import com.jogamp.newt.Screen;
import com.jogamp.newt.javafx.NewtCanvasJFX;
import com.jogamp.newt.opengl.GLWindow;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.util.Animator;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class NewtCanvaJFXSnippet extends Application
{

	public static void main(final String[] args)
	{
		launch();
	}

	private Animator animator;

	@Override
	public void start(final Stage stage)
	{
		Platform.setImplicitExit(true);
		final Group g = new Group();

		final Scene scene = new Scene(g, 800, 600);
		stage.setScene(scene);
		stage.show();

		final com.jogamp.newt.Display jfxNewtDisplay = NewtFactory.createDisplay(null, false);
		final Screen screen = NewtFactory.createScreen(jfxNewtDisplay, 0);
		final GLCapabilities caps = new GLCapabilities(GLProfile.getMaxFixedFunc(true));
		final GLWindow glWindow = GLWindow.create(screen, caps);
		glWindow.addGLEventListener(new GLEventListenerImpl());

		// When user right-click on Circle
		final NewtCanvasJFX glCanvas = new NewtCanvasJFX(glWindow);

		glCanvas.setWidth(800);
		glCanvas.setHeight(600);

		g.getChildren().add(glCanvas);

		glCanvas.widthProperty().bind(stage.widthProperty());
		glCanvas.heightProperty().bind(stage.heightProperty());

		this.animator = new Animator(glWindow);
		this.animator.start();
	}

	@Override
	public void stop() throws Exception
	{
		if (this.animator != null)
		{
			this.animator.stop();
		}
	}
}